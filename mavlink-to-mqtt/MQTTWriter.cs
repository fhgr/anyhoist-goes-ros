﻿using Asv.Mavlink.V2.Common;
using Asv.Mavlink;
using MQTTnet.Client;
using MQTTnet;
using System.Text.Json.Serialization;
using System.Text.Json;
using Asv.Mavlink.V2.Fhgr;
using System.Diagnostics;
using System.Reactive.Concurrency;
using Microsoft.Extensions.Configuration;

namespace Mav2MQTT
{
    public class MQTTWriter : IObserver<IPacketV2<IPayload>>
    {
        private readonly string _vehicleId;
        private readonly IMqttClient _mqttClient;
        private readonly MqttApplicationMessageBuilder _mqttApplicationMessageBuilder;
        private readonly JsonSerializerOptions _jsonSerializerOptions;

        public MQTTWriter(string vehicleId, IMqttClient mqttClient, IConfigurationRoot configuration)
        {
            _vehicleId = vehicleId;
            _mqttClient = mqttClient;
            _mqttApplicationMessageBuilder = new MqttApplicationMessageBuilder();
            _jsonSerializerOptions = new JsonSerializerOptions
            {
                NumberHandling = JsonNumberHandling.AllowReadingFromString | JsonNumberHandling.AllowNamedFloatingPointLiterals
            };
        }

        public void OnCompleted() { }

        public void OnError(Exception error)
        {
            Console.WriteLine($"{_vehicleId}: {error.Message}");
        }

        public async void OnNext(IPacketV2<IPayload> packet)
        {
            // Discard Odometery, Ping and Timesync and Heartbeat Packets for now
            if (packet is OdometryPacket || packet is PingPacket || packet is TimesyncPacket || packet is HeartbeatPacket)
            {
                return;
            }

            var serializedPayload = JsonSerializer.SerializeToUtf8Bytes(packet.Payload, packet.Payload.GetType(), _jsonSerializerOptions);
            var applicationMessage = _mqttApplicationMessageBuilder
                .WithTopic($"{_vehicleId}/{packet.Name}")
                .WithPayload(serializedPayload)
                .Build();

            await _mqttClient.PublishAsync(applicationMessage, CancellationToken.None);
        }
    }

}
