﻿using Asv.Mavlink;
using Asv.Mavlink.V2.Ardupilotmega;
using Asv.Mavlink.V2.Fhgr;
using Mav2MQTT;
using Microsoft.Extensions.Configuration;
using MQTTnet;
using MQTTnet.Client;
using Newtonsoft.Json;
using System.Reactive.Linq;

var configuration = new ConfigurationBuilder().AddCommandLine(args).AddEnvironmentVariables().Build();

const string VehiclesPath = "./vehicles";
var mavlinkConnectionStrings = configuration.GetValue("mavlinkConnectionStrings", "udp://0.0.0.0:14540");
var mqttBrokerAddress = configuration.GetValue("mqttBrokerAddress", "192.168.1.20");
var mqttBrokerPort = configuration.GetValue("mqttBrokerPort", 1883);

if (Directory.Exists(VehiclesPath))
{
    const string TelemetryBokehKey = "Telemetry Bokeh";
    var jsonFiles = Directory.EnumerateFiles(VehiclesPath, "*.json");
    var vehicleConfigs = jsonFiles.Select(p => File.ReadAllText(p)).Select(p => JsonConvert.DeserializeObject<VehicleConfig>(p));
    if (vehicleConfigs.Any())
    {
        mavlinkConnectionStrings = string.Join(";", vehicleConfigs.Select(p => $"udp://127.0.0.1:{p!.Endpoints.First(p => p.Value == TelemetryBokehKey).Key}"));
        Console.WriteLine($"Overriding supplied mavlinkConnectionStrings with vehicles from folder {VehiclesPath}");
        Console.WriteLine($"New {nameof(mavlinkConnectionStrings)}: {mavlinkConnectionStrings}");
    }
}

if (string.IsNullOrEmpty(mavlinkConnectionStrings))
{
    Console.WriteLine("Required parameter 'mavlinkConnectionStrings' not specified!");
    return;
}

if (string.IsNullOrEmpty(mqttBrokerAddress))
{
    Console.WriteLine("Required parameter 'mqttBrokerAddress' not specified!");
    return;
}

if (mqttBrokerPort <= 0)
{
    Console.WriteLine("Required parameter 'mqttBrokerPort' not specified!");
    return;
}

var mqttFactory = new MqttFactory();
var mqttClient = mqttFactory.CreateMqttClient();
var mqttClientOptions = new MqttClientOptionsBuilder().WithTcpServer(mqttBrokerAddress, mqttBrokerPort).WithTimeout(TimeSpan.FromSeconds(10)).Build();
var connectionResult = await mqttClient.ConnectAsync(mqttClientOptions, CancellationToken.None);

if (connectionResult.ResultCode != MqttClientConnectResultCode.Success)
{
    Console.WriteLine($"Connection to MQTT Broker failed with status {connectionResult.ResultCode}");
    return;
}



var mavlinkConnections = mavlinkConnectionStrings.Split(";")
    .Select(p => new MavlinkV2Connection(p, r =>
    {
        MavlinkV2Connection.RegisterDefaultDialects(r);
        r.RegisterFhgrDialect();
        r.OutError.Subscribe(p =>
        {
            Console.WriteLine(p.Message);
        });
    }))
    .ToList();

foreach (var connection in mavlinkConnections)
{
    var connectionId = connection.DataStream.Name.Split(":")[1];

    // special case if dynamic port with rhost and rport
    if (connectionId.Contains("=>"))
    {
        connectionId = connectionId.Split("=>")[0];
    }

    Console.WriteLine($"Creating MQTTWriter for connection {connection.DataStream.Name}");

    var writer = new MQTTWriter(connectionId, mqttClient, configuration);
    connection.Subscribe(writer);
}

while (true)
{
    await Task.Delay(TimeSpan.FromSeconds(1));
}
