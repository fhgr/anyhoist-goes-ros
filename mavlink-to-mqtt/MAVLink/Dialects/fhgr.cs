// MIT License
//
// Copyright (c) 2023 asv-soft (https://github.com/asv-soft)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

// This code was generate by tool Asv.Mavlink.Shell version 3.2.5-alpha-11

using System;
using System.Text;
using Asv.Mavlink.V2.Common;
using Asv.IO;

namespace Asv.Mavlink.V2.Fhgr
{

    public static class FhgrHelper
    {
        public static void RegisterFhgrDialect(this IPacketDecoder<IPacketV2<IPayload>> src)
        {
            src.Register(()=>new RadiationDetectorDataPacket());
            src.Register(()=>new ChemicalDetectorDataPacket());
            src.Register(()=>new Css45CommonDataPacket());
            src.Register(()=>new Css45SpectrumData0Packet());
            src.Register(()=>new Css45SpectrumData1Packet());
            src.Register(()=>new Css45SpectrumData2Packet());
            src.Register(()=>new Css45SpectrumData3Packet());
            src.Register(()=>new Css45SpectrumData4Packet());
            src.Register(()=>new Css45SpectrumData5Packet());
            src.Register(()=>new Css45SpectrumData6Packet());
            src.Register(()=>new Css45SpectrumData7Packet());
        }
    }

#region Enums


#endregion

#region Messages

    /// <summary>
    /// Radiation Detector Data
    ///  RADIATION_DETECTOR_DATA
    /// </summary>
    public class RadiationDetectorDataPacket: PacketV2<RadiationDetectorDataPayload>
    {
	    public const int PacketMessageId = 14203;
        public override int MessageId => PacketMessageId;
        public override byte GetCrcEtra() => 50;
        public override bool WrapToV2Extension => false;

        public override RadiationDetectorDataPayload Payload { get; } = new RadiationDetectorDataPayload();

        public override string Name => "RADIATION_DETECTOR_DATA";
    }

    /// <summary>
    ///  RADIATION_DETECTOR_DATA
    /// </summary>
    public class RadiationDetectorDataPayload : IPayload
    {
        public byte GetMaxByteSize() => 25; // Sum of byte sized of all fields (include extended)
        public byte GetMinByteSize() => 25; // of byte sized of fields (exclude extended)
        public int GetByteSize()
        {
            var sum = 0;
            sum+=4; //DetectorTimeMs
            sum+=4; //DeltaTS
            sum+=4; //Counts0
            sum+=4; //Counts1
            sum+=4; //Rates0
            sum+=4; //Rates1
            sum+=1; //IsEnabled
            return (byte)sum;
        }



        public void Deserialize(ref ReadOnlySpan<byte> buffer)
        {
            DetectorTimeMs = BinSerialize.ReadUInt(ref buffer);
            DeltaTS = BinSerialize.ReadFloat(ref buffer);
            Counts0 = BinSerialize.ReadUInt(ref buffer);
            Counts1 = BinSerialize.ReadUInt(ref buffer);
            Rates0 = BinSerialize.ReadFloat(ref buffer);
            Rates1 = BinSerialize.ReadFloat(ref buffer);
            IsEnabled = (byte)BinSerialize.ReadByte(ref buffer);

        }

        public void Serialize(ref Span<byte> buffer)
        {
            BinSerialize.WriteUInt(ref buffer,DetectorTimeMs);
            BinSerialize.WriteFloat(ref buffer,DeltaTS);
            BinSerialize.WriteUInt(ref buffer,Counts0);
            BinSerialize.WriteUInt(ref buffer,Counts1);
            BinSerialize.WriteFloat(ref buffer,Rates0);
            BinSerialize.WriteFloat(ref buffer,Rates1);
            BinSerialize.WriteByte(ref buffer,(byte)IsEnabled);
            /* PayloadByteSize = 25 */;
        }
        
        



        /// <summary>
        /// Time since system boot
        /// OriginName: detector_time_ms, Units: ms, IsExtended: false
        /// </summary>
        public uint DetectorTimeMs { get; set; }
        /// <summary>
        /// The time that has passed since the last measurement has been sent.
        /// OriginName: delta_t_s, Units: s, IsExtended: false
        /// </summary>
        public float DeltaTS { get; set; }
        /// <summary>
        /// An array of detector counts (accumulated since start). First element stands for all the counts from low to mid cut.
        /// OriginName: counts_0, Units: , IsExtended: false
        /// </summary>
        public uint Counts0 { get; set; }
        /// <summary>
        /// Second is for the values that are in the bins from mid to high cut.
        /// OriginName: counts_1, Units: , IsExtended: false
        /// </summary>
        public uint Counts1 { get; set; }
        /// <summary>
        /// An array containing the rate below and above the mid cut. The rate unit is counts per second normalized from the counted neutrons in the time difference delta_t_s
        /// OriginName: rates_0, Units: counts/s, IsExtended: false
        /// </summary>
        public float Rates0 { get; set; }
        /// <summary>
        /// An array containing the rate below and above the mid cut. The rate unit is counts per second normalized from the counted neutrons in the time difference delta_t_s
        /// OriginName: rates_1, Units: counts/s, IsExtended: false
        /// </summary>
        public float Rates1 { get; set; }
        /// <summary>
        /// Set this value to 0xFF
        /// OriginName: is_enabled, Units: , IsExtended: false
        /// </summary>
        public byte IsEnabled { get; set; }
    }
    /// <summary>
    /// Chemical Detector Data
    ///  CHEMICAL_DETECTOR_DATA
    /// </summary>
    public class ChemicalDetectorDataPacket: PacketV2<ChemicalDetectorDataPayload>
    {
	    public const int PacketMessageId = 14202;
        public override int MessageId => PacketMessageId;
        public override byte GetCrcEtra() => 164;
        public override bool WrapToV2Extension => false;

        public override ChemicalDetectorDataPayload Payload { get; } = new ChemicalDetectorDataPayload();

        public override string Name => "CHEMICAL_DETECTOR_DATA";
    }

    /// <summary>
    ///  CHEMICAL_DETECTOR_DATA
    /// </summary>
    public class ChemicalDetectorDataPayload : IPayload
    {
        public byte GetMaxByteSize() => 13; // Sum of byte sized of all fields (include extended)
        public byte GetMinByteSize() => 13; // of byte sized of fields (exclude extended)
        public int GetByteSize()
        {
            var sum = 0;
            sum+=4; //Concentration
            sum+=4; //Dose
            sum+=1; //AgentId
            sum+=1; //Bars
            sum+=1; //BarsPeak
            sum+=1; //HazardLevel
            sum+=1; //IsEnabled
            return (byte)sum;
        }



        public void Deserialize(ref ReadOnlySpan<byte> buffer)
        {
            Concentration = BinSerialize.ReadFloat(ref buffer);
            Dose = BinSerialize.ReadFloat(ref buffer);
            AgentId = (byte)BinSerialize.ReadByte(ref buffer);
            Bars = (byte)BinSerialize.ReadByte(ref buffer);
            BarsPeak = (byte)BinSerialize.ReadByte(ref buffer);
            HazardLevel = (byte)BinSerialize.ReadByte(ref buffer);
            IsEnabled = (byte)BinSerialize.ReadByte(ref buffer);

        }

        public void Serialize(ref Span<byte> buffer)
        {
            BinSerialize.WriteFloat(ref buffer,Concentration);
            BinSerialize.WriteFloat(ref buffer,Dose);
            BinSerialize.WriteByte(ref buffer,(byte)AgentId);
            BinSerialize.WriteByte(ref buffer,(byte)Bars);
            BinSerialize.WriteByte(ref buffer,(byte)BarsPeak);
            BinSerialize.WriteByte(ref buffer,(byte)HazardLevel);
            BinSerialize.WriteByte(ref buffer,(byte)IsEnabled);
            /* PayloadByteSize = 13 */;
        }
        
        



        /// <summary>
        /// Concentration (mg/m3), IEEE floating point format
        /// OriginName: concentration, Units: mg/m3, IsExtended: false
        /// </summary>
        public float Concentration { get; set; }
        /// <summary>
        /// Dose (mg-min/m3), IEEE floating point format
        /// OriginName: dose, Units: mg-min/m3, IsExtended: false
        /// </summary>
        public float Dose { get; set; }
        /// <summary>
        /// Agent ID, 0=No agent, 1=GA, 2=GB, 3=GD/GF, 4=VX, 5=VXR, 6=DPM, 7=AC/CK, 8=CK, 9=AC, 11=HD
        /// OriginName: agent_id, Units: , IsExtended: false
        /// </summary>
        public byte AgentId { get; set; }
        /// <summary>
        /// Bars (0 - 8)
        /// OriginName: bars, Units: , IsExtended: false
        /// </summary>
        public byte Bars { get; set; }
        /// <summary>
        /// Peak Bars (0 - 8)
        /// OriginName: bars_peak, Units: , IsExtended: false
        /// </summary>
        public byte BarsPeak { get; set; }
        /// <summary>
        /// Hazard Level - none, low, medium, high ( 0-3 )
        /// OriginName: hazard_level, Units: , IsExtended: false
        /// </summary>
        public byte HazardLevel { get; set; }
        /// <summary>
        /// Set this value to 0xFF
        /// OriginName: is_enabled, Units: , IsExtended: false
        /// </summary>
        public byte IsEnabled { get; set; }
    }
    /// <summary>
    /// CSS45 Common Data
    ///  CSS45_COMMON_DATA
    /// </summary>
    public class Css45CommonDataPacket: PacketV2<Css45CommonDataPayload>
    {
	    public const int PacketMessageId = 14204;
        public override int MessageId => PacketMessageId;
        public override byte GetCrcEtra() => 47;
        public override bool WrapToV2Extension => false;

        public override Css45CommonDataPayload Payload { get; } = new Css45CommonDataPayload();

        public override string Name => "CSS45_COMMON_DATA";
    }

    /// <summary>
    ///  CSS45_COMMON_DATA
    /// </summary>
    public class Css45CommonDataPayload : IPayload
    {
        public byte GetMaxByteSize() => 16; // Sum of byte sized of all fields (include extended)
        public byte GetMinByteSize() => 16; // of byte sized of fields (exclude extended)
        public int GetByteSize()
        {
            var sum = 0;
            sum+=4; //Ev
            sum+=4; //Es
            sum+=4; //Ee
            sum+=4; //Cct
            return (byte)sum;
        }



        public void Deserialize(ref ReadOnlySpan<byte> buffer)
        {
            Ev = BinSerialize.ReadFloat(ref buffer);
            Es = BinSerialize.ReadFloat(ref buffer);
            Ee = BinSerialize.ReadFloat(ref buffer);
            Cct = BinSerialize.ReadFloat(ref buffer);

        }

        public void Serialize(ref Span<byte> buffer)
        {
            BinSerialize.WriteFloat(ref buffer,Ev);
            BinSerialize.WriteFloat(ref buffer,Es);
            BinSerialize.WriteFloat(ref buffer,Ee);
            BinSerialize.WriteFloat(ref buffer,Cct);
            /* PayloadByteSize = 16 */;
        }
        
        



        /// <summary>
        /// Illuminance Photopic
        /// OriginName: ev, Units: lx, IsExtended: false
        /// </summary>
        public float Ev { get; set; }
        /// <summary>
        /// Illuminance Scotopic
        /// OriginName: es, Units: lx, IsExtended: false
        /// </summary>
        public float Es { get; set; }
        /// <summary>
        /// Irradiance
        /// OriginName: ee, Units: W/m2, IsExtended: false
        /// </summary>
        public float Ee { get; set; }
        /// <summary>
        /// CCT
        /// OriginName: cct, Units: K, IsExtended: false
        /// </summary>
        public float Cct { get; set; }
    }
    /// <summary>
    /// CSS45 Spectrum Data 0
    ///  CSS45_SPECTRUM_DATA_0
    /// </summary>
    public class Css45SpectrumData0Packet: PacketV2<Css45SpectrumData0Payload>
    {
	    public const int PacketMessageId = 14210;
        public override int MessageId => PacketMessageId;
        public override byte GetCrcEtra() => 184;
        public override bool WrapToV2Extension => false;

        public override Css45SpectrumData0Payload Payload { get; } = new Css45SpectrumData0Payload();

        public override string Name => "CSS45_SPECTRUM_DATA_0";
    }

    /// <summary>
    ///  CSS45_SPECTRUM_DATA_0
    /// </summary>
    public class Css45SpectrumData0Payload : IPayload
    {
        public byte GetMaxByteSize() => 240; // Sum of byte sized of all fields (include extended)
        public byte GetMinByteSize() => 240; // of byte sized of fields (exclude extended)
        public int GetByteSize()
        {
            var sum = 0;
            sum+=4; //WavelengthFrom
            sum+=4; //WavelengthTo
            sum+=Spectrum.Length * 4; //Spectrum
            return (byte)sum;
        }



        public void Deserialize(ref ReadOnlySpan<byte> buffer)
        {
            var arraySize = 0;
            var payloadSize = buffer.Length;
            WavelengthFrom = BinSerialize.ReadUInt(ref buffer);
            WavelengthTo = BinSerialize.ReadUInt(ref buffer);
            arraySize = /*ArrayLength*/58 - Math.Max(0,((/*PayloadByteSize*/240 - payloadSize - /*ExtendedFieldsLength*/0)/4 /*FieldTypeByteSize*/));
            Spectrum = new float[arraySize];
            for(var i=0;i<arraySize;i++)
            {
                Spectrum[i] = BinSerialize.ReadFloat(ref buffer);
            }

        }

        public void Serialize(ref Span<byte> buffer)
        {
            BinSerialize.WriteUInt(ref buffer,WavelengthFrom);
            BinSerialize.WriteUInt(ref buffer,WavelengthTo);
            for(var i=0;i<Spectrum.Length;i++)
            {
                BinSerialize.WriteFloat(ref buffer,Spectrum[i]);
            }
            /* PayloadByteSize = 240 */;
        }
        
        



        /// <summary>
        /// At which wavelength the spectrum starts
        /// OriginName: wavelength_from, Units: lx, IsExtended: false
        /// </summary>
        public uint WavelengthFrom { get; set; }
        /// <summary>
        /// At which wavelength the spectrum ends
        /// OriginName: wavelength_to, Units: lx, IsExtended: false
        /// </summary>
        public uint WavelengthTo { get; set; }
        /// <summary>
        /// Spectral Irradiance Lower
        /// OriginName: spectrum, Units: W/(m2nm), IsExtended: false
        /// </summary>
        public float[] Spectrum { get; set; } = new float[58];
        public byte GetSpectrumMaxItemsCount() => 58;
    }
    /// <summary>
    /// CSS45 Spectrum Data 1
    ///  CSS45_SPECTRUM_DATA_1
    /// </summary>
    public class Css45SpectrumData1Packet: PacketV2<Css45SpectrumData1Payload>
    {
	    public const int PacketMessageId = 14211;
        public override int MessageId => PacketMessageId;
        public override byte GetCrcEtra() => 76;
        public override bool WrapToV2Extension => false;

        public override Css45SpectrumData1Payload Payload { get; } = new Css45SpectrumData1Payload();

        public override string Name => "CSS45_SPECTRUM_DATA_1";
    }

    /// <summary>
    ///  CSS45_SPECTRUM_DATA_1
    /// </summary>
    public class Css45SpectrumData1Payload : IPayload
    {
        public byte GetMaxByteSize() => 240; // Sum of byte sized of all fields (include extended)
        public byte GetMinByteSize() => 240; // of byte sized of fields (exclude extended)
        public int GetByteSize()
        {
            var sum = 0;
            sum+=4; //WavelengthFrom
            sum+=4; //WavelengthTo
            sum+=Spectrum.Length * 4; //Spectrum
            return (byte)sum;
        }



        public void Deserialize(ref ReadOnlySpan<byte> buffer)
        {
            var arraySize = 0;
            var payloadSize = buffer.Length;
            WavelengthFrom = BinSerialize.ReadUInt(ref buffer);
            WavelengthTo = BinSerialize.ReadUInt(ref buffer);
            arraySize = /*ArrayLength*/58 - Math.Max(0,((/*PayloadByteSize*/240 - payloadSize - /*ExtendedFieldsLength*/0)/4 /*FieldTypeByteSize*/));
            Spectrum = new float[arraySize];
            for(var i=0;i<arraySize;i++)
            {
                Spectrum[i] = BinSerialize.ReadFloat(ref buffer);
            }

        }

        public void Serialize(ref Span<byte> buffer)
        {
            BinSerialize.WriteUInt(ref buffer,WavelengthFrom);
            BinSerialize.WriteUInt(ref buffer,WavelengthTo);
            for(var i=0;i<Spectrum.Length;i++)
            {
                BinSerialize.WriteFloat(ref buffer,Spectrum[i]);
            }
            /* PayloadByteSize = 240 */;
        }
        
        



        /// <summary>
        /// At which wavelength the spectrum starts
        /// OriginName: wavelength_from, Units: lx, IsExtended: false
        /// </summary>
        public uint WavelengthFrom { get; set; }
        /// <summary>
        /// At which wavelength the spectrum ends
        /// OriginName: wavelength_to, Units: lx, IsExtended: false
        /// </summary>
        public uint WavelengthTo { get; set; }
        /// <summary>
        /// Spectral Irradiance Lower
        /// OriginName: spectrum, Units: W/(m2nm), IsExtended: false
        /// </summary>
        public float[] Spectrum { get; set; } = new float[58];
        public byte GetSpectrumMaxItemsCount() => 58;
    }
    /// <summary>
    /// CSS45 Spectrum Data 2
    ///  CSS45_SPECTRUM_DATA_2
    /// </summary>
    public class Css45SpectrumData2Packet: PacketV2<Css45SpectrumData2Payload>
    {
	    public const int PacketMessageId = 14212;
        public override int MessageId => PacketMessageId;
        public override byte GetCrcEtra() => 81;
        public override bool WrapToV2Extension => false;

        public override Css45SpectrumData2Payload Payload { get; } = new Css45SpectrumData2Payload();

        public override string Name => "CSS45_SPECTRUM_DATA_2";
    }

    /// <summary>
    ///  CSS45_SPECTRUM_DATA_2
    /// </summary>
    public class Css45SpectrumData2Payload : IPayload
    {
        public byte GetMaxByteSize() => 240; // Sum of byte sized of all fields (include extended)
        public byte GetMinByteSize() => 240; // of byte sized of fields (exclude extended)
        public int GetByteSize()
        {
            var sum = 0;
            sum+=4; //WavelengthFrom
            sum+=4; //WavelengthTo
            sum+=Spectrum.Length * 4; //Spectrum
            return (byte)sum;
        }



        public void Deserialize(ref ReadOnlySpan<byte> buffer)
        {
            var arraySize = 0;
            var payloadSize = buffer.Length;
            WavelengthFrom = BinSerialize.ReadUInt(ref buffer);
            WavelengthTo = BinSerialize.ReadUInt(ref buffer);
            arraySize = /*ArrayLength*/58 - Math.Max(0,((/*PayloadByteSize*/240 - payloadSize - /*ExtendedFieldsLength*/0)/4 /*FieldTypeByteSize*/));
            Spectrum = new float[arraySize];
            for(var i=0;i<arraySize;i++)
            {
                Spectrum[i] = BinSerialize.ReadFloat(ref buffer);
            }

        }

        public void Serialize(ref Span<byte> buffer)
        {
            BinSerialize.WriteUInt(ref buffer,WavelengthFrom);
            BinSerialize.WriteUInt(ref buffer,WavelengthTo);
            for(var i=0;i<Spectrum.Length;i++)
            {
                BinSerialize.WriteFloat(ref buffer,Spectrum[i]);
            }
            /* PayloadByteSize = 240 */;
        }
        
        



        /// <summary>
        /// At which wavelength the spectrum starts
        /// OriginName: wavelength_from, Units: lx, IsExtended: false
        /// </summary>
        public uint WavelengthFrom { get; set; }
        /// <summary>
        /// At which wavelength the spectrum ends
        /// OriginName: wavelength_to, Units: lx, IsExtended: false
        /// </summary>
        public uint WavelengthTo { get; set; }
        /// <summary>
        /// Spectral Irradiance Lower
        /// OriginName: spectrum, Units: W/(m2nm), IsExtended: false
        /// </summary>
        public float[] Spectrum { get; set; } = new float[58];
        public byte GetSpectrumMaxItemsCount() => 58;
    }
    /// <summary>
    /// CSS45 LSpectrum Data 3
    ///  CSS45_SPECTRUM_DATA_3
    /// </summary>
    public class Css45SpectrumData3Packet: PacketV2<Css45SpectrumData3Payload>
    {
	    public const int PacketMessageId = 14213;
        public override int MessageId => PacketMessageId;
        public override byte GetCrcEtra() => 165;
        public override bool WrapToV2Extension => false;

        public override Css45SpectrumData3Payload Payload { get; } = new Css45SpectrumData3Payload();

        public override string Name => "CSS45_SPECTRUM_DATA_3";
    }

    /// <summary>
    ///  CSS45_SPECTRUM_DATA_3
    /// </summary>
    public class Css45SpectrumData3Payload : IPayload
    {
        public byte GetMaxByteSize() => 240; // Sum of byte sized of all fields (include extended)
        public byte GetMinByteSize() => 240; // of byte sized of fields (exclude extended)
        public int GetByteSize()
        {
            var sum = 0;
            sum+=4; //WavelengthFrom
            sum+=4; //WavelengthTo
            sum+=Spectrum.Length * 4; //Spectrum
            return (byte)sum;
        }



        public void Deserialize(ref ReadOnlySpan<byte> buffer)
        {
            var arraySize = 0;
            var payloadSize = buffer.Length;
            WavelengthFrom = BinSerialize.ReadUInt(ref buffer);
            WavelengthTo = BinSerialize.ReadUInt(ref buffer);
            arraySize = /*ArrayLength*/58 - Math.Max(0,((/*PayloadByteSize*/240 - payloadSize - /*ExtendedFieldsLength*/0)/4 /*FieldTypeByteSize*/));
            Spectrum = new float[arraySize];
            for(var i=0;i<arraySize;i++)
            {
                Spectrum[i] = BinSerialize.ReadFloat(ref buffer);
            }

        }

        public void Serialize(ref Span<byte> buffer)
        {
            BinSerialize.WriteUInt(ref buffer,WavelengthFrom);
            BinSerialize.WriteUInt(ref buffer,WavelengthTo);
            for(var i=0;i<Spectrum.Length;i++)
            {
                BinSerialize.WriteFloat(ref buffer,Spectrum[i]);
            }
            /* PayloadByteSize = 240 */;
        }
        
        



        /// <summary>
        /// At which wavelength the spectrum starts
        /// OriginName: wavelength_from, Units: lx, IsExtended: false
        /// </summary>
        public uint WavelengthFrom { get; set; }
        /// <summary>
        /// At which wavelength the spectrum ends
        /// OriginName: wavelength_to, Units: lx, IsExtended: false
        /// </summary>
        public uint WavelengthTo { get; set; }
        /// <summary>
        /// Spectral Irradiance Lower
        /// OriginName: spectrum, Units: W/(m2nm), IsExtended: false
        /// </summary>
        public float[] Spectrum { get; set; } = new float[58];
        public byte GetSpectrumMaxItemsCount() => 58;
    }
    /// <summary>
    /// CSS45 Spectrum Data 4
    ///  CSS45_SPECTRUM_DATA_4
    /// </summary>
    public class Css45SpectrumData4Packet: PacketV2<Css45SpectrumData4Payload>
    {
	    public const int PacketMessageId = 14214;
        public override int MessageId => PacketMessageId;
        public override byte GetCrcEtra() => 107;
        public override bool WrapToV2Extension => false;

        public override Css45SpectrumData4Payload Payload { get; } = new Css45SpectrumData4Payload();

        public override string Name => "CSS45_SPECTRUM_DATA_4";
    }

    /// <summary>
    ///  CSS45_SPECTRUM_DATA_4
    /// </summary>
    public class Css45SpectrumData4Payload : IPayload
    {
        public byte GetMaxByteSize() => 240; // Sum of byte sized of all fields (include extended)
        public byte GetMinByteSize() => 240; // of byte sized of fields (exclude extended)
        public int GetByteSize()
        {
            var sum = 0;
            sum+=4; //WavelengthFrom
            sum+=4; //WavelengthTo
            sum+=Spectrum.Length * 4; //Spectrum
            return (byte)sum;
        }



        public void Deserialize(ref ReadOnlySpan<byte> buffer)
        {
            var arraySize = 0;
            var payloadSize = buffer.Length;
            WavelengthFrom = BinSerialize.ReadUInt(ref buffer);
            WavelengthTo = BinSerialize.ReadUInt(ref buffer);
            arraySize = /*ArrayLength*/58 - Math.Max(0,((/*PayloadByteSize*/240 - payloadSize - /*ExtendedFieldsLength*/0)/4 /*FieldTypeByteSize*/));
            Spectrum = new float[arraySize];
            for(var i=0;i<arraySize;i++)
            {
                Spectrum[i] = BinSerialize.ReadFloat(ref buffer);
            }

        }

        public void Serialize(ref Span<byte> buffer)
        {
            BinSerialize.WriteUInt(ref buffer,WavelengthFrom);
            BinSerialize.WriteUInt(ref buffer,WavelengthTo);
            for(var i=0;i<Spectrum.Length;i++)
            {
                BinSerialize.WriteFloat(ref buffer,Spectrum[i]);
            }
            /* PayloadByteSize = 240 */;
        }
        
        



        /// <summary>
        /// At which wavelength the spectrum starts
        /// OriginName: wavelength_from, Units: lx, IsExtended: false
        /// </summary>
        public uint WavelengthFrom { get; set; }
        /// <summary>
        /// At which wavelength the spectrum ends
        /// OriginName: wavelength_to, Units: lx, IsExtended: false
        /// </summary>
        public uint WavelengthTo { get; set; }
        /// <summary>
        /// Spectral Irradiance Lower
        /// OriginName: spectrum, Units: W/(m2nm), IsExtended: false
        /// </summary>
        public float[] Spectrum { get; set; } = new float[58];
        public byte GetSpectrumMaxItemsCount() => 58;
    }
    /// <summary>
    /// CSS45 Spectrum Data 5
    ///  CSS45_SPECTRUM_DATA_5
    /// </summary>
    public class Css45SpectrumData5Packet: PacketV2<Css45SpectrumData5Payload>
    {
	    public const int PacketMessageId = 14215;
        public override int MessageId => PacketMessageId;
        public override byte GetCrcEtra() => 159;
        public override bool WrapToV2Extension => false;

        public override Css45SpectrumData5Payload Payload { get; } = new Css45SpectrumData5Payload();

        public override string Name => "CSS45_SPECTRUM_DATA_5";
    }

    /// <summary>
    ///  CSS45_SPECTRUM_DATA_5
    /// </summary>
    public class Css45SpectrumData5Payload : IPayload
    {
        public byte GetMaxByteSize() => 240; // Sum of byte sized of all fields (include extended)
        public byte GetMinByteSize() => 240; // of byte sized of fields (exclude extended)
        public int GetByteSize()
        {
            var sum = 0;
            sum+=4; //WavelengthFrom
            sum+=4; //WavelengthTo
            sum+=Spectrum.Length * 4; //Spectrum
            return (byte)sum;
        }



        public void Deserialize(ref ReadOnlySpan<byte> buffer)
        {
            var arraySize = 0;
            var payloadSize = buffer.Length;
            WavelengthFrom = BinSerialize.ReadUInt(ref buffer);
            WavelengthTo = BinSerialize.ReadUInt(ref buffer);
            arraySize = /*ArrayLength*/58 - Math.Max(0,((/*PayloadByteSize*/240 - payloadSize - /*ExtendedFieldsLength*/0)/4 /*FieldTypeByteSize*/));
            Spectrum = new float[arraySize];
            for(var i=0;i<arraySize;i++)
            {
                Spectrum[i] = BinSerialize.ReadFloat(ref buffer);
            }

        }

        public void Serialize(ref Span<byte> buffer)
        {
            BinSerialize.WriteUInt(ref buffer,WavelengthFrom);
            BinSerialize.WriteUInt(ref buffer,WavelengthTo);
            for(var i=0;i<Spectrum.Length;i++)
            {
                BinSerialize.WriteFloat(ref buffer,Spectrum[i]);
            }
            /* PayloadByteSize = 240 */;
        }
        
        



        /// <summary>
        /// At which wavelength the spectrum starts
        /// OriginName: wavelength_from, Units: lx, IsExtended: false
        /// </summary>
        public uint WavelengthFrom { get; set; }
        /// <summary>
        /// At which wavelength the spectrum ends
        /// OriginName: wavelength_to, Units: lx, IsExtended: false
        /// </summary>
        public uint WavelengthTo { get; set; }
        /// <summary>
        /// Spectral Irradiance Lower
        /// OriginName: spectrum, Units: W/(m2nm), IsExtended: false
        /// </summary>
        public float[] Spectrum { get; set; } = new float[58];
        public byte GetSpectrumMaxItemsCount() => 58;
    }
    /// <summary>
    /// CSS45 Spectrum Data 6
    ///  CSS45_SPECTRUM_DATA_6
    /// </summary>
    public class Css45SpectrumData6Packet: PacketV2<Css45SpectrumData6Payload>
    {
	    public const int PacketMessageId = 14216;
        public override int MessageId => PacketMessageId;
        public override byte GetCrcEtra() => 130;
        public override bool WrapToV2Extension => false;

        public override Css45SpectrumData6Payload Payload { get; } = new Css45SpectrumData6Payload();

        public override string Name => "CSS45_SPECTRUM_DATA_6";
    }

    /// <summary>
    ///  CSS45_SPECTRUM_DATA_6
    /// </summary>
    public class Css45SpectrumData6Payload : IPayload
    {
        public byte GetMaxByteSize() => 240; // Sum of byte sized of all fields (include extended)
        public byte GetMinByteSize() => 240; // of byte sized of fields (exclude extended)
        public int GetByteSize()
        {
            var sum = 0;
            sum+=4; //WavelengthFrom
            sum+=4; //WavelengthTo
            sum+=Spectrum.Length * 4; //Spectrum
            return (byte)sum;
        }



        public void Deserialize(ref ReadOnlySpan<byte> buffer)
        {
            var arraySize = 0;
            var payloadSize = buffer.Length;
            WavelengthFrom = BinSerialize.ReadUInt(ref buffer);
            WavelengthTo = BinSerialize.ReadUInt(ref buffer);
            arraySize = /*ArrayLength*/58 - Math.Max(0,((/*PayloadByteSize*/240 - payloadSize - /*ExtendedFieldsLength*/0)/4 /*FieldTypeByteSize*/));
            Spectrum = new float[arraySize];
            for(var i=0;i<arraySize;i++)
            {
                Spectrum[i] = BinSerialize.ReadFloat(ref buffer);
            }

        }

        public void Serialize(ref Span<byte> buffer)
        {
            BinSerialize.WriteUInt(ref buffer,WavelengthFrom);
            BinSerialize.WriteUInt(ref buffer,WavelengthTo);
            for(var i=0;i<Spectrum.Length;i++)
            {
                BinSerialize.WriteFloat(ref buffer,Spectrum[i]);
            }
            /* PayloadByteSize = 240 */;
        }
        
        



        /// <summary>
        /// At which wavelength the spectrum starts
        /// OriginName: wavelength_from, Units: lx, IsExtended: false
        /// </summary>
        public uint WavelengthFrom { get; set; }
        /// <summary>
        /// At which wavelength the spectrum ends
        /// OriginName: wavelength_to, Units: lx, IsExtended: false
        /// </summary>
        public uint WavelengthTo { get; set; }
        /// <summary>
        /// Spectral Irradiance Lower
        /// OriginName: spectrum, Units: W/(m2nm), IsExtended: false
        /// </summary>
        public float[] Spectrum { get; set; } = new float[58];
        public byte GetSpectrumMaxItemsCount() => 58;
    }
    /// <summary>
    /// CSS45 Spectrum Data 7
    ///  CSS45_SPECTRUM_DATA_7
    /// </summary>
    public class Css45SpectrumData7Packet: PacketV2<Css45SpectrumData7Payload>
    {
	    public const int PacketMessageId = 14217;
        public override int MessageId => PacketMessageId;
        public override byte GetCrcEtra() => 118;
        public override bool WrapToV2Extension => false;

        public override Css45SpectrumData7Payload Payload { get; } = new Css45SpectrumData7Payload();

        public override string Name => "CSS45_SPECTRUM_DATA_7";
    }

    /// <summary>
    ///  CSS45_SPECTRUM_DATA_7
    /// </summary>
    public class Css45SpectrumData7Payload : IPayload
    {
        public byte GetMaxByteSize() => 240; // Sum of byte sized of all fields (include extended)
        public byte GetMinByteSize() => 240; // of byte sized of fields (exclude extended)
        public int GetByteSize()
        {
            var sum = 0;
            sum+=4; //WavelengthFrom
            sum+=4; //WavelengthTo
            sum+=Spectrum.Length * 4; //Spectrum
            return (byte)sum;
        }



        public void Deserialize(ref ReadOnlySpan<byte> buffer)
        {
            var arraySize = 0;
            var payloadSize = buffer.Length;
            WavelengthFrom = BinSerialize.ReadUInt(ref buffer);
            WavelengthTo = BinSerialize.ReadUInt(ref buffer);
            arraySize = /*ArrayLength*/58 - Math.Max(0,((/*PayloadByteSize*/240 - payloadSize - /*ExtendedFieldsLength*/0)/4 /*FieldTypeByteSize*/));
            Spectrum = new float[arraySize];
            for(var i=0;i<arraySize;i++)
            {
                Spectrum[i] = BinSerialize.ReadFloat(ref buffer);
            }

        }

        public void Serialize(ref Span<byte> buffer)
        {
            BinSerialize.WriteUInt(ref buffer,WavelengthFrom);
            BinSerialize.WriteUInt(ref buffer,WavelengthTo);
            for(var i=0;i<Spectrum.Length;i++)
            {
                BinSerialize.WriteFloat(ref buffer,Spectrum[i]);
            }
            /* PayloadByteSize = 240 */;
        }
        
        



        /// <summary>
        /// At which wavelength the spectrum starts
        /// OriginName: wavelength_from, Units: lx, IsExtended: false
        /// </summary>
        public uint WavelengthFrom { get; set; }
        /// <summary>
        /// At which wavelength the spectrum ends
        /// OriginName: wavelength_to, Units: lx, IsExtended: false
        /// </summary>
        public uint WavelengthTo { get; set; }
        /// <summary>
        /// Spectral Irradiance Lower
        /// OriginName: spectrum, Units: W/(m2nm), IsExtended: false
        /// </summary>
        public float[] Spectrum { get; set; } = new float[58];
        public byte GetSpectrumMaxItemsCount() => 58;
    }


#endregion


}
