﻿using Asv.Mavlink.V2.Ardupilotmega;
using Asv.Mavlink.V2.Common;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mav2MQTT.Extensions
{
    public static class Uid2Extensions
    {
        public static string ToBase64RFC4648_5(this byte[] bytes)
        {
            var uid2Base64 = Convert.ToBase64String(bytes, 0, bytes.Length);
            var uid2Base64RFC4648_5 = uid2Base64.Replace("+", "-").Replace("/", "_");
            return uid2Base64RFC4648_5;
        }
    }
}

// SAMPLE CODE FOR REQUESTING UUID2
//var packet = new AutopilotVersionRequestPacket();
//packet.Payload.TargetSystem = 1;
//packet.Payload.TargetComponent = 1;

//Console.WriteLine($"Requesting UID2 for {connection.DataStream.Name}...");
//var autopilotVersionPacket = await connection.SendAndWaitAnswer<AutopilotVersionPacket, AutopilotVersionPayload>(packet, 1, 1, CancellationToken.None);
//var vehicleId = autopilotVersionPacket.Payload.Uid2.ToBase64RFC4648_5();
//var process = Process.Start("C:\\fhgr\\Downloads\\mavsdk-windows-x64-release\\bin\\mavsdk_server_bin.exe", "udp://127.0.0.1:15555");
//process.Start();



//Console.WriteLine($"VehicleId (from UID2) for {connection.DataStream.Name} is {vehicleId}");
