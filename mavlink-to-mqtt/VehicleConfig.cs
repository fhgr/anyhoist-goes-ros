﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mav2MQTT
{
    public class VehicleConfig
    {
        public string IpAddress { get; set; } = default!;
        public string Name { get; set; } = default!;
        public bool IsEnabled { get; set; }
        public int MavsdkGrpcPort { get; set; }

        public Dictionary<int, string> Endpoints { get; set; } = default!;
    }
}
