import time
from bokeh.plotting import figure, curdoc, row, column
from bokeh.models import Switch, CustomJS, Button, Div, TabPanel, Tabs, ColumnDataSource, DatetimeTickFormatter, Range1d
from mqtt.mqtt_client import MqttClient

# start this server with command:
# bokeh serve example-ui.py

mqtt_client = MqttClient()

doc = curdoc()

# first example scatter plot, static data
scatter_plot = figure(sizing_mode="stretch_width")
scatter_plot.circle([1, 2, 3, 4, 5], [6, 7, 2, 4, 5], size=20, color="navy", alpha=0.5)

# second example line plot, static data
line_plot = figure(sizing_mode="stretch_width")
line_plot.line([1, 2, 3, 4, 5], [6, 7, 2, 4, 5], line_width=2)

# third example line plot, dynamic data using a ColumnDataSource which data can be streamed to
line_plot_stream_example = figure(sizing_mode="stretch_width")
line_plot_stream_example_cds = ColumnDataSource({"x": [], "battery_remaining": []})
line_plot_stream_example.y_range = Range1d(0, 100)
line_plot_stream_example.xaxis.formatter = DatetimeTickFormatter(syncable=False, microseconds = "%H:%M:%S", milliseconds = "%H:%M:%S", seconds = "%H:%M:%S", minutes = "%H:%M:%S", hours = "%H:%M:%S", days="%H:%M:%S",months="%H:%M:%S", years="%H:%M:%S",  minsec = "%H:%M:%S" )
line_plot_stream_example.line("x", "battery_remaining", source =  line_plot_stream_example_cds, legend_label="Battery Remaining (%)", line_width=2)

button1 = Button(label="Click me button 1", height=50, width=140, button_type="primary")
button2 = Button(label="DONT CLICK button 2", height=50, width=140, button_type="danger")

# this button simulates a received MQTT message
def button1_on_click():
    mqtt_client.update("0/BATTERY_STATUS", "{\"CurrentConsumed\":0,\"EnergyConsumed\":0,\"Temperature\":0,\"Voltages\":[0,0,0,0,0,0,0,0,0,0],\"CurrentBattery\":0,\"Id\":1,\"BatteryFunction\":1,\"Type\":1,\"BatteryRemaining\":53,\"TimeRemaining\":0,\"ChargeState\":2}")
    print("button 1 pressed")

# this button simulates a received MQTT message
def button2_on_click():
    mqtt_client.update("0/BATTERY_STATUS", "{\"CurrentConsumed\":0,\"EnergyConsumed\":0,\"Temperature\":0,\"Voltages\":[0,0,0,0,0,0,0,0,0,0],\"CurrentBattery\":0,\"Id\":1,\"BatteryFunction\":1,\"Type\":1,\"BatteryRemaining\":44,\"TimeRemaining\":0,\"ChargeState\":2}")
    print("button 2 pressed")

button1.on_click(button1_on_click)
button2.on_click(button2_on_click)

row1 = row(scatter_plot, line_plot, sizing_mode="stretch_both")
row2 = row(line_plot_stream_example, sizing_mode="stretch_both")
row3 = row(button1, button2, sizing_mode="stretch_both")

my_column = column(row1, row2, row3, sizing_mode="stretch_both")

# example BATTERY_STATUS "{\"CurrentConsumed\":0,\"EnergyConsumed\":0,\"Temperature\":0,\"Voltages\":[0,0,0,0,0,0,0,0,0,0],\"CurrentBattery\":0,\"Id\":1,\"BatteryFunction\":1,\"Type\":1,\"BatteryRemaining\":0,\"TimeRemaining\":0,\"ChargeState\":2}")
mqtt_client.update("0/BATTERY_STATUS", "{\"CurrentConsumed\":0,\"EnergyConsumed\":0,\"Temperature\":0,\"Voltages\":[0,0,0,0,0,0,0,0,0,0],\"CurrentBattery\":0,\"Id\":1,\"BatteryFunction\":1,\"Type\":1,\"BatteryRemaining\":80,\"TimeRemaining\":0,\"ChargeState\":2}")


# here you can update the UI on a periodic timer. for example, stream battery_remaining every 100ms
def tick():
    x = int(time.time()*1000)

    # update live battery plot with newest message we got via MQTT
    battery_status = mqtt_client.get_message("BATTERY_STATUS", "0")
    battery_remaining = battery_status.get("BatteryRemaining")
    line_plot_stream_example_cds.stream({"x": [x], "battery_remaining": [battery_remaining]}, rollover=100)

# instead of streaming data this way, you can also connect to the mqtt_client running locally and publish data this way
# WARN: the mqtt_client.publish method was NOT tested and probably does not work, it's just an idea
# def tick():
    # mqtt_client.publish("0", "{\"CurrentConsumed\":0,\"EnergyConsumed\":0,\"Temperature\":0,\"Voltages\":[0,0,0,0,0,0,0,0,0,0],\"CurrentBattery\":0,\"Id\":1,\"BatteryFunction\":1,\"Type\":1,\"BatteryRemaining\":80,\"TimeRemaining\":0,\"ChargeState\":2}")


# mqtt_client.connect()

doc.add_root(my_column)
doc.add_periodic_callback(tick, 100)