import paho.mqtt.client as mqtt
from paho.mqtt.client import MQTTMessage
import json
import time

class MqttClient(object):
    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(MqttClient, cls).__new__(cls)
            cls.instance.__initialized = False
        return cls.instance

    def __init__(self):
        if self.__initialized:
            return
        self.__initialized = True

        ip = "127.0.0.1"
        port = 1883
        auto_connect = False

        self.__messages = {}
        self.__is_running = False
        self.client = mqtt.Client()
        self.__is_connected = False
        self.client.on_connect = self.__on_connect
        self.client.on_message = self.__on_message
        self.__ip = ip
        self.__port = port

        if auto_connect:
            self.client.connect(ip, port, 60)
            self.client.loop_start()
            self.__is_connected = True

    def isRunning(self):
        return self.__is_running
    

    def stop(self):
        self.__is_running = False
        self.client.disconnect()
        self.client.loop_stop()
        self.__is_connected = False

    def connect(self):
        if self.__is_connected:
            return True
        try:
            self.client.connect(self.__ip, self.__port, 60)
            self.client.loop_start()
            self.__is_connected = True
            self.__is_running = True
            self.__messages = {}
            return True
        except:
            return False

    def update(self, topic: str, value: str) -> None:
        try:
            self.__messages[topic] = json.loads(value)
        except:
            print(f"JSON Parse error: {value}")
            pass

    def publish(self, topic: str, value: str) -> None:
        self.client.publish(topic, value)

    def get_message(self, message_name: str, vehicle_id):
        return self.__messages.get(f"{vehicle_id}/{message_name}") or {}

    def __on_connect(self, client: mqtt.Client, userdata, flags, rc):
        client.subscribe([
            ("+/ATTITUDE", 0),
            ("+/ALTITUDE", 0),
            ("+/GLOBAL_POSITION_INT", 0),
            ("+/GPS_RAW_INT", 0),
            ("+/DISTANCE_SENSOR", 0),
            ("+/WIND_COV", 0),
            ("+/BATTERY_STATUS", 0),
            ("+/CHEMICAL_DETECTOR_DATA", 0),
            ("+/RADIATION_DETECTOR_DATA", 0),
            ])

    def __on_message(self, client, userdata, msg: MQTTMessage):
        try:
            if self.__is_discovering:
                self.__parse_vehicle_id(msg.topic)
                return
            self.update(msg.topic, msg.payload.decode())
        except UnicodeEncodeError:
            print("Error: could not decode MQTT message 'msg.payload'")
            pass