# anyhoist-goes-ros


# Prerequisites
 - git
 - Docker
 - Python

# Installation
- `git clone https://git.fhgr.ch/armasuisse_fhgr/anyhoist-goes-ros.git`
- `pip install -r requirements.txt`
- `docker volume create grafana-data`
- `docker volume create influxdb-config`
- `docker volume create influxdb-data`
- `docker compose up -d`
- `bokeh serve example-ui.py --dev`
